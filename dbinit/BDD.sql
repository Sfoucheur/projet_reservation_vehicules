CREATE TABLE `Voiture` (
  `id_vehicule` int PRIMARY KEY AUTO_INCREMENT,
  `modele` int,
  `marque` int,
  `plaque_immatriculation` varchar(8),
  `description` varchar(500),
  `image` varchar(150),
  `kilometrage` int,
  `plein_essence` boolean
);

CREATE TABLE `Reservation` (
  `id_reservation` int PRIMARY KEY AUTO_INCREMENT,
  `id_user_resa` int,
  `id_vehicule` int,
  `conducteur` int,
  `date_debut` datetime,
  `date_fin` datetime,
  `commentaire` varchar(2000),
  `is_delete` boolean
);

CREATE TABLE `Membre` (
  `id_membre` int PRIMARY KEY,
  `nom` varchar(100),
  `prenom` varchar(100),
  `poste` varchar(250),
  `telephone` int(10),
  `email` varchar(100)
);

CREATE TABLE `Participant` (
  `id_membre` int,
  `id_reservation` int,
  PRIMARY KEY (`id_membre`, `id_reservation`)
);

CREATE TABLE `Marque` (
  `id_marque` int PRIMARY KEY AUTO_INCREMENT,
  `label` varchar(100)
);

CREATE TABLE `Modele` (
  `id_modele` int PRIMARY KEY AUTO_INCREMENT,
  `label` varchar(100)
);

ALTER TABLE `Voiture` ADD FOREIGN KEY (`modele`) REFERENCES `Modele` (`id_modele`);

ALTER TABLE `Voiture` ADD FOREIGN KEY (`marque`) REFERENCES `Marque` (`id_marque`);

ALTER TABLE `Reservation` ADD FOREIGN KEY (`id_user_resa`) REFERENCES `Membre` (`id_membre`);

ALTER TABLE `Reservation` ADD FOREIGN KEY (`id_vehicule`) REFERENCES `Voiture` (`id_vehicule`);

ALTER TABLE `Reservation` ADD FOREIGN KEY (`conducteur`) REFERENCES `Membre` (`id_membre`);

ALTER TABLE `Participant` ADD FOREIGN KEY (`id_membre`) REFERENCES `Membre` (`id_membre`);

ALTER TABLE `Participant` ADD FOREIGN KEY (`id_reservation`) REFERENCES `Reservation` (`id_reservation`) ON DELETE CASCADE;

INSERT INTO `Marque` (`id_marque`, `label`) VALUES (NULL, 'Renault'); 
INSERT INTO `Marque` (`id_marque`, `label`) VALUES (NULL, 'BMW'); 
INSERT INTO `Marque` (`id_marque`, `label`) VALUES (NULL, 'Peugeot');
INSERT INTO `Modele` (`id_modele`, `label`) VALUES (NULL, 'Clio 4');
INSERT INTO `Modele` (`id_modele`, `label`) VALUES (NULL, 'X5');
INSERT INTO `Modele` (`id_modele`, `label`) VALUES (NULL, '207'); 
INSERT INTO `Voiture` (`id_vehicule`, `modele`, `marque`, `plaque_immatriculation`, `description`, `image`, `kilometrage`, `plein_essence`) VALUES (NULL, '1', '1', 'AB895BA', 'Renault Clio 4', 'assets/img/clio4.jpg', '88888', '1');
INSERT INTO `Voiture` (`id_vehicule`, `modele`, `marque`, `plaque_immatriculation`, `description`, `image`, `kilometrage`, `plein_essence`) VALUES (NULL, '2', '2', 'ZZ999ZZ', 'BMW X5', 'assets/img/x5.jpg', '395', '0');
INSERT INTO `Voiture` (`id_vehicule`, `modele`, `marque`, `plaque_immatriculation`, `description`, `image`, `kilometrage`, `plein_essence`) VALUES (NULL, '3', '3', 'GH568ML', 'Peugeot 207', 'assets/img/207.jpg', '113455', '1'); 

INSERT INTO `Membre` (`id_membre`, `nom`, `prenom`, `poste`, `telephone`, `email`) VALUES
(1, 'QUINIOU', 'GILDAS', NULL, NULL, NULL),
(2, 'AUBRY', 'MATISSE', NULL, NULL, NULL),
(3, 'BERNASCONI', 'PAUL', NULL, NULL, NULL),
(4, 'BOISTUAUD', 'HUGO', NULL, NULL, NULL),
(5, 'BOULC\'H', 'ANTHONY', NULL, NULL, NULL),
(6, 'BULTEAU', 'GAEL', NULL, NULL, NULL),
(7, 'DESBOIS', 'MATHIS', NULL, NULL, NULL),
(8, 'FORESTIER', 'CYNTHIA', NULL, NULL, NULL),
(9, 'FOUCHEUR', 'SEBASTIEN', NULL, NULL, NULL),
(10, 'GEFFROY', 'ROMAIN', NULL, NULL, NULL),
(11, 'GICQUEL', 'PIERRE', NULL, NULL, NULL),
(12, 'LE DEUNFF', 'ADRIEN', NULL, NULL, NULL),
(13, 'LE REST', 'ROMAIN', NULL, NULL, NULL),
(14, 'LEBREUILLY', 'FABIEN', NULL, NULL, NULL),
(15, 'MERRIEN', 'GAETAN', NULL, NULL, NULL),
(16, 'MOURIEC', 'ALEXANDRE', NULL, NULL, NULL),
(17, 'NIZERY', 'ALEXANDRE', NULL, NULL, NULL),
(18, 'SIMONET', 'FLORENT', NULL, NULL, NULL);