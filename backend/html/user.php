<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

//User Login
/**
 *  @OA\Post(
 *      path="/user/login",
 *      tags={"Authentication"},
 *      @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(@OA\Items(
 *              type="object",
 *              required={"login","password"}
 *          ),
 *          ref="#/components/schemas/User")
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="Success",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="Login Success")  
 *          )   
 *      ),
 *      @OA\Response(
 *          response="500",
 *          description="Error",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="Login Error")  
 *          )   
 *      )
 * )
*/
$app->post('/user/login', function (Request $request, Response $response, array $args) {
    session_start();
    $values = $request->getParsedBody();
    $fd = fopen('https://sso.lpweb-lannion.fr/api/v1.7/login?user='.$values['username'].'&password='.$values['password'], 'r');
        $texte="";
        while($ligne = fgets($fd)) {
            $texte .= $ligne;
        }
    $json = json_decode($texte,JSON_PRETTY_PRINT);
    $result = [];
    if($json['ret']==0){
        $result['reponse']="Login error";
    }else{
        $result['reponse']="Login success";
        $_SESSION['user']=$json['token'];
    }
    $response->getBody()->write(json_encode($result));
    return $response;
                   
    
});
$app->options('/user/login', function (Request $request, Response $response): Response {
    return $response;
});

// User Logout
/**
 *  @OA\Get(
 *      path="/user/logout",
 *      tags={"Authentication"},
 *      @OA\Response(
 *          response="200",
 *          description="Success",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="Logout Success")  
 *          )   
 *      ),
 *      @OA\Response(
 *          response="500",
 *          description="Error",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="Logout Error")  
 *          )   
 *      )
 * 
 * )
*/
$app->get('/user/logout', function (Request $request, Response $response, array $args) {
    session_start();
    if(isLogged()){
        fopen('https://sso.lpweb-lannion.fr/api/v1.7/logout?token='.$_SESSION['user'], 'r');
        unset($_SESSION['user']);
        $result['reponse']="Logout success";
    }
    else{
        $result['reponse']='Logout error';
    }
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->options('/user/logout', function (Request $request, Response $response): Response {
    return $response;
});
// Change User Password
$app->post('/user/password', function (Request $request, Response $response, array $args) {
    $values = $request->getParsedBody();
    session_start();
    if($token = $_SESSION['user']){
        $fd = popen("curl -d '{'old_password':".$values['old_password'].", 'new_password':".$values['new_password']."}' -H 'Content-Type: application/json' -X POST https://sso.lpweb-lannion.fr/api/v1.7/password?token='".$token,"r");
        $texte = '';
        while ($line=fgets($fd)) {
            $texte .= $line;
        }
        pclose($fd);
        $json = json_decode($texte,JSON_PRETTY_PRINT);
        if($ret = $json['ret']){
            $result['reponse']=$ret;
        }else{
            $result['reponse']="Password Change success";
        }
    }
    else{
        $result['reponse']='Password Change error normal';
    }
    $response->getBody()->write(json_encode($result));
    return $response;
});
$app->options('/user/password', function (Request $request, Response $response): Response {
    return $response;
});
$app->get('/user/checkSession', function (Request $request, Response $response): Response {
    if(isLogged())
        $response->getBody()->write(json_encode(["reponse"=>"ok"]));
    else
        $response->getBody()->write(json_encode(["reponse"=>"nok"]));
    return $response;
});
$app->options('/user/checkSession', function (Request $request, Response $response): Response {
    return $response;
});
$app->get('/user/whoIs', function (Request $request, Response $response): Response {
    if(isLogged())
    {
        $fd = fopen('https://sso.lpweb-lannion.fr/api/v1.7/who_is?token='.$_SESSION['user'],'r');
        $texte="";
        while($ligne = fgets($fd)) {
            $texte .= $ligne;
        }
        $json = json_decode($texte,JSON_PRETTY_PRINT);
        if($json['ret']==0){
            $response->getBody()->write(json_encode(["reponse"=>"nok"]));
        }else{
            $response->getBody()->write(json_encode(["prenom"=>$json['prenom'],"id"=>$json['id']]));
        }
    }  
    else{
        $response->getBody()->write(json_encode(["reponse"=>"nok"]));
    }    
    return $response;
});
$app->options('/user/whoIs', function (Request $request, Response $response): Response {
    return $response;
});
/*$app->get('/user/all', function (Request $request, Response $response): Response {
    if(isLogged())
    {
        $bdd = getPDO();
        $insert_users = $bdd->prepare("INSERT INTO `Membre` (`id_membre`, `nom`, `prenom`) VALUES (?, ?, ?);");
        $fd = fopen('https://sso.lpweb-lannion.fr/api/v1.7/users?token='.$_SESSION['user'],'r');
        $texte="";
        while($ligne = fgets($fd)) {
            $texte .= $ligne;
        }
        $json = json_decode($texte,JSON_PRETTY_PRINT);
        foreach($json as $people){
            $insert_users->execute(array($people['id'],$people['id'],$people['prenom']));
        }
        $response->getBody()->write(json_encode([$json]));
        
    }  
    else{
        $response->getBody()->write(json_encode(["reponse"=>"nok"]));
    }    
    return $response;
});
$app->options('/user/all', function (Request $request, Response $response): Response {
    return $response;
});*/
$app->get('/user/all', function (Request $request, Response $response): Response {
    if(isLogged())
    {
        $bdd = getPDO();
        $all_users = $bdd->query("SELECT `id_membre`, `nom`, `prenom` FROM `Membre`;");
        $users = $all_users->fetchAll(PDO::FETCH_ASSOC);
        $response->getBody()->write(json_encode($users));
    }  
    else{
        $response->getBody()->write(json_encode(["reponse"=>"nok"]));
    }    
    return $response;
});
$app->options('/user/all', function (Request $request, Response $response): Response {
    return $response;
});





?>