<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Cette API permet d'ajouter les paticipants qui ont participés au créneau (méthode POST).
 * Lien GET : http://localhost:8001/add_participant/{nom_membre}/{id_reservation}
 * Explication : Cette url permet de faire un POST pour ajouter les particpants qui accompagnaient le conducteur lors de la réservation du véhicule.
 * Pour réaliser cette opération il est nécessaire de préciser dans l'URL :
 * {nom_membre} : Le nom du membre accompaganteur (string),
 * {id_reservation} : L'id de la réservation auquel a participé le membre (INT).
 */
/**
 *  @OA\Post(
 *      path="/add_participant/{nom_membre}/{id_reservation}",
 *      tags={"Participant"},
 *      @OA\Parameter(
 *          name="nom_membre",
 *          in="path",
 *          description="Le nom du membre",
 *          required=true,
 *          @OA\Schema(type="string")
 *      ),
 *      @OA\Parameter(
 *          name="id_reservation",
 *          in="path",
 *          description="ID de reservation",
 *          required=true,
 *          @OA\Schema(type="integer")
 *      ),
 *      @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(@OA\Items(
 *              type="object",
 *              required={"nomMembre","idMembre"}
 *          ),
 *          ref="#/components/schemas/Participant")
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="Success",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="Login Success")  
 *          )   
 *      ),
 *      @OA\Response(
 *          response="500",
 *          description="Error",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="Login Error")  
 *          )   
 *      )
 * )
*/
$app->post('/add_participant/{nom_membre}/{id_reservation}', function (Request $request, Response $response, array $args) {
    if(isLogged()){
        $bdd = getPDO();
        /** Check id membre */
        $recape_id_membre = $bdd->prepare("SELECT id_membre FROM Membre WHERE nom = ?");
        $recape_id_membre->execute(array($args['nom_membre']));
        $res = $recape_id_membre->fetch(PDO::FETCH_ASSOC);

        /** Insert membre in particpant of reservation */
        $insert_id_membre = $bdd->prepare("INSERT INTO Participant (id_membre,id_reservation) VALUES(?,?);");
        $insert_id_membre->execute(array($res['id_membre'],$args['id_reservation'])); 


        $payload = json_encode(['OK' => $res],JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
    }
    else{
        $response->getBody()->write(json_encode(['reponse' => 'Error']));
    }
    
    return $response->withHeader('Content-Type','application/json');

});
$app->options('/add_participant/{nom_membre}/{id_reservation}', function (Request $request, Response $response, array $args) {
    return $response;
});