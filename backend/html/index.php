<?php
    use Psr\Http\Message\ResponseInterface as Response;
    use Psr\Http\Message\ServerRequestInterface as Request;
    use Slim\Factory\AppFactory;
    use Slim\Exception\HttpNotFoundException;
    use OpenApi\Annotations as OA;
    require __DIR__ . '/../vendor/autoload.php';

    $app = AppFactory::create();
    function getPDO(){
        return new PDO('mysql:host=mysql;dbname=voiture', "seb", "lol123");
    }
    function isLogged(){
        session_start();
        if($token = $_SESSION['user']){
            $fd = fopen('https://sso.lpweb-lannion.fr/api/v1.7/check_access/vehicules?token='.$token, 'r');
            $texte="";
            while($ligne = fgets($fd)) {
                $texte .= $ligne;
            }
            $json = json_decode($texte,JSON_PRETTY_PRINT);
            return $json['ret']=='granted';
        }
        else{
            return false;
        }
    }
    $app->addBodyParsingMiddleware();
    $app->addRoutingMiddleware();
    $app->addErrorMiddleware(true,true,true);
    
    
    $app->add(function ($request, $handler) {
        $response = $handler->handle($request);
        return $response
            ->withHeader('Access-Control-Allow-Origin', 'http://localhost:4200')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, access-control-allow-origin,Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS')
            ->withHeader('Access-Control-Allow-Credentials', 'true');
    });
    
    $app->get('/v1', function (Request $request,Response $response, $args) {
        $response->getBody()->write("Michel world!");
    return $response;
    });
    require_once('user.php');
    require_once('reservation.php');
    require_once('voiture.php');
    require_once('participant.php');

    
    $app->run();

?>