<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Cette API permet d'afficher les voitures (méthode GET).
 * Lien GET : http://localhost:8001/voitures
 * Explication : Cette url permet de faire un GET sur les voitures de notre base de donnée. 
 * Cette méthode récupère : 
 * - Le modele de la voiture,
 * - La marque,
 * - La descritpion,
 * - L'image,
 * - Le kilométrage,
 * - Le plein d'essence (si celui-ci a été fait ou non)
 */
/**
 *  @OA\Get(
 *      path="/voitures",
 *      tags={"Voitures"},
 *      @OA\Response(
 *          response="200",
 *          description="Success",
 *          @OA\JsonContent(
 *            type="array",
 *            @OA\Items(ref="#/components/schemas/Voiture")
 *          )   
 *      ),
 *      @OA\Response(
 *          response="500",
 *          description="Error",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="ERR")  
 *          )   
 *      )
 * )
*/
$app->get('/voitures', function (Request $request, Response $response, array $args) {
    if(isLogged()){
        $bdd = getPDO();
        $recape_Voitures = $bdd->query("SELECT mo.label as modele, ma.label marque, v.description, v.image, v.kilometrage, v.plein_essence, v.id_vehicule 
                                        FROM Voiture v INNER JOIN Marque ma on v.marque=ma.id_marque 
                                        INNER JOIN Modele mo on v.modele=mo.id_modele");
        $voitures = array();
        $voitures = $recape_Voitures->fetchAll(PDO::FETCH_OBJ);
        $payload = json_encode($voitures, JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
    }
    else{
        $response->getBody()->write(json_encode(['reponse' => "Err"]));
    }
    return $response->withHeader('Content-Type','application/json');
});
$app->options('/voitures', function (Request $request, Response $response, array $args) {
    return $response;
});


/**
 * Cette API permet d'afficher les voitures réservées par un utilisateur précis (méthode GET).
 * Lien GET : http://localhost:8001/voitures/{id_user_resa}
 * Explication : Cette url permet de faire un GET sur les voitures réservées par un utilisateur.
 * Pour se faire il faut préciser :
 * - {id_user_resa} : qui est un id utilisateur (INT).
 */
/**
 *  @OA\Get(
 *      path="/voitures/{id_user_resa}",
 *      tags={"Voitures"},
 *      @OA\Parameter(
 *          name="id_user_resa",
 *          in="path",
 *          description="ID utilisateur",
 *          required=true,
 *          @OA\Schema(type="integer")
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="Success",
 *          @OA\JsonContent(
 *            type="array",
 *            @OA\Items(ref="#/components/schemas/Voiture")
 *          )   
 *      ),
 *      @OA\Response(
 *          response="500",
 *          description="Error",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="ERR")  
 *          )   
 *      )
 * )
*/
$app->get('/voitures/{id_user_resa}', function (Request $request, Response $response, array $args) {
    if(isLogged()){
        $bdd = getPDO();
        /** Recape voiture id with user id in reservation table */
        $recape_resa_user = $bdd->prepare("SELECT id_vehicule FROM Reservation WHERE id_user_resa = ?");
        $recape_resa_user->execute(array($args['id_user_resa']));


        $voitures = array();

        foreach($recape_resa_user as $key=>$value){
            /** recape voiture */
            $recape_voiture = $bdd->prepare("SELECT modele, marque, description, image, kilometrage, plein_essence FROM Voiture WHERE id_vehicule = ?");
            $recape_voiture->execute(array($value['id_vehicule']));
            $res = $recape_voiture->fetch(PDO::FETCH_ASSOC);
            array_push($voitures,$res);
        }

        $payload = json_encode($voitures, JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
    }
    else{
        $response->getBody()->write(json_encode(['reponse' => 'Error']));
    }
    return $response->withHeader('Content-Type','application/json');
});
$app->options('/voitures/{id_user_resa}/', function (Request $request, Response $response, array $args) {
    return $response;
});



/**
 * Cette API permet d'afficher les voitures réservées par un utilisateur précis (méthode GET).
 * Lien GET : http://localhost:8001/voitures/{id_vehicule}/{plein}
 * Explication : Cette url permet de faire un GET sur les voitures réservées par un utilisateur.
 * Pour se faire il faut préciser :
 * - {id_user_resa} : qui est un id utilisateur (INT).
 */
/**
 *  @OA\Put(
 *      path="/voitures/{id_vehicule}/{plein}",
 *      tags={"Voitures"},
 *      @OA\Parameter(
 *          name="id_vehicule",
 *          in="path",
 *          description="ID Vehicule",
 *          required=true,
 *          @OA\Schema(type="integer")
 *      ),
 *      @OA\Parameter(
 *          name="plein",
 *          in="path",
 *          description="Boolean plein_essence",
 *          required=true,
 *          @OA\Schema(type="boolean")
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="Success",
 *          @OA\JsonContent(
 *            type="array",
 *            @OA\Items(ref="#/components/schemas/Voiture")
 *          )   
 *      ),
 *      @OA\Response(
 *          response="500",
 *          description="Error",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="ERR")  
 *          )   
 *      )
 * )
*/
$app->put('/voitures/{id_vehicule}/{plein_essence}', function (Request $request, Response $response, array $args) {
    if(isLogged()){
        $bdd = getPDO();

        if($args['plein_essence'] == 'true'){
            $updatePlein = $bdd->prepare("UPDATE `Voiture` SET `plein_essence`= 1 WHERE id_vehicule = ?");
            $updatePlein->execute(array($args['id_vehicule']));
        }else{
            $updatePlein = $bdd->prepare("UPDATE `Voiture` SET `plein_essence`= 0 WHERE id_vehicule = ?");
            $updatePlein->execute(array($args['id_vehicule']));
        }

        if($updatePlein){
            $payload = json_encode(['code' => 'OK'],JSON_PRETTY_PRINT);
        }else{
            $payload = json_encode(['code' => 'ERR'],JSON_PRETTY_PRINT);
        }

        $response->getBody()->write($payload);
    }
    else{
        $response->getBody()->write(json_encode(['reponse' => 'Error']));
    }
    
    return $response->withHeader('Content-Type','application/json');
});
$app->options('/voitures/{plein_essence}', function (Request $request, Response $response, array $args) {
    return $response;
});
$app->get('/voiture/{id_voiture}', function (Request $request, Response $response, array $args) {
    if(isLogged()){
        $bdd = getPDO();
        $recape_Voitures = $bdd->prepare("SELECT mo.label as modele, ma.label marque, v.description, v.image, v.kilometrage, v.plein_essence, v.id_vehicule 
                                        FROM Voiture v INNER JOIN Marque ma on v.marque=ma.id_marque 
                                        INNER JOIN Modele mo on v.modele=mo.id_modele
                                        WHERE v.id_vehicule=?");
        $voitures = array();
        $recape_Voitures->execute(array($args['id_voiture']));
        $voitures = $recape_Voitures->fetch(PDO::FETCH_ASSOC);
        $payload = json_encode($voitures, JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
    }
    else{
        $response->getBody()->write(json_encode(['reponse' => "Err"]));
    }
    return $response->withHeader('Content-Type','application/json');
});
$app->options('/voiture/{id_voiture}', function (Request $request, Response $response, array $args) {
    return $response;
});







