<?php
    use OpenApi\Annotations as OA;
    /**
     * @OA\Info(title="API ResaVehicules", version="0.1")
     * @OA\Server(
     *  url="http://sebastien.lpweb-lannion.fr:8001",
     *  description = "Api de réservation de véhicules"
     * )
     */
    /**
     * @OA\Schema(
     *  description="User Model",
     *  title="User Model",
     *  required={"login","password"},
     * )
     */
    class User {
        /**
         * @OA\Property(type="string",nullable=false, description="Nom d'utilisateur")
         */
        public $login;
        /**
         * @OA\Property(type="string",nullable=false,description="Mot de passe de l'utilisateur")
         */
        public $password;
    }
    /**
     * @OA\Schema(
     *  description="Participant Model",
     *  title="Participant Model",
     *  required={"nomMembre","idMembre"},
     * )
     */
    class Participant {
        /**
         * @OA\Property(type="string",nullable=false, description="Nom du Participant")
         */
        public $nomMembre;
        /**
         * @OA\Property(type="integer",nullable=false,description="ID de reservation")
         */
        public $idMembre;
    }
    /**
     * @OA\Schema(
     *      description="Reservation Model",
     *      title="Reservation Model",
     *      required={"IDvehicule", "IDconducteur","dateDebut","dateFin"},
     * )
     */
    class Reservation {
        /**
         * @OA\Property(type="integer",nullable=false, description="ID vehicule")
         */
        public $IDvehicule;
        /**
         * @OA\Property(type="integer",nullable=false,description="ID conducteur")
         */
        public $IDconducteur;
        /**
         * @OA\Property(type="string",nullable=false,format="date-time",description="Debut reservation")
         */
        public $dateDebut;
        /**
         * @OA\Property(type="string",nullable=false,format="date-time",description="Fin reservation")
         */
        public $dateFin;
    }
    /**
     * @OA\Schema(
     *  description="Reservation Put Model",
     *  title="Reservation Put Model",
     *  required={"IDreservation"},
     * 
     * )
     */
    class ReservationPut {
        /**
         * @OA\Property(type="integer",nullable=false, description="ID reservation")
         */
        public $IDreservation;
        /**
        /**
         * @OA\Property(type="integer",nullable=false, description="ID vehicule")
         */
        public $IDvehicule;
        /**
         * @OA\Property(type="integer",nullable=false,description="ID conducteur")
         */
        public $IDconducteur;
        /**
         * @OA\Property(type="string",nullable=false,format="date-time",description="Debut reservation")
         */
        public $dateDebut;
        /**
         * @OA\Property(type="string",nullable=false,format="date-time",description="Fin reservation")
         */
        public $dateFin;
    }
    /**
     * @OA\Schema(
     *  description="Voiture Model",
     *  title="Voiture Model"
     * )
     */
    class Voiture{
        /**
         * @OA\Property(type="string",nullable=false, description="Modele Voiture")
         */
        public $modele;
        /**
        /**
         * @OA\Property(type="string",nullable=false, description="Marque Voiture")
         */
        public $marque;
        /**
         * @OA\Property(type="string",nullable=true,description="Description Voiture")
         */
        public $description;
        /**
         * @OA\Property(type="string",nullable=false,description="Image Voiture")
         */
        public $image;
        /**
         * @OA\Property(type="integer",nullable=false,description="Kilometrage Voiture")
         */
        public $kilometrage;
        /**
         * @OA\Property(type="boolean",nullable=false,description="Plein fait ou non")
         */
        public $plein_essence;
    }
?>