<?php
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

/**
 * Cette fonction permet de créer une réservation (méthode POST).
 * Lien POST : http://localhost:8001/reservation/{vehicule}/{date_debut}/{date_fin}/{commentaire}
 * Explication : Cette url permet de faire un POST. Pour réaliser cette opération il est nécessaire de préciser dans l'URL :
 * {véhicule} : Un numéro de véhicule (INT),
 * {date_debut} : Une date de début (DATE TIME = aaaa-mm-jj),
 * {date_fin} : Une date de fin (DATE TIME = aaaa-mm-jj),
 * {commentaire} : Enfin il faut ajouter un commentaire (qui peut-être NULL).
 */

 /**
 *  @OA\Post(
 *      path="/add_reservation/{vehicule}/{conducteur}/{date_debut}/{date_fin}",
 *      tags={"Reservation"},
 *      @OA\Parameter(
 *          name="vehicule",
 *          in="path",
 *          description="ID vehicule",
 *          required=true,
 *          @OA\Schema(type="integer")
 *      ),
 *      @OA\Parameter(
 *          name="conducteur",
 *          in="path",
 *          description="ID conducteur",
 *          required=true,
 *          @OA\Schema(type="integer")
 *      ),
 *      @OA\Parameter(
 *          name="date_debut",
 *          in="path",
 *          description="Debut reservation",
 *          required=true,
 *          @OA\Schema(type="string")
 *      ),
 *      @OA\Parameter(
 *          name="date_fin",
 *          in="path",
 *          description="Fin reservation",
 *          required=true,
 *          @OA\Schema(type="string")
 *      ),
 *      @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(@OA\Items(
 *              type="object",
 *              required={"IDvehicule","IDconducteur","dateDebut","dateFin"}
 *          ),
 *          ref="#/components/schemas/Reservation")
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="Success",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="Login Success")  
 *          )   
 *      ),
 *      @OA\Response(
 *          response="500",
 *          description="Error",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="Login Error")  
 *          )   
 *      )
 * )
*/
$app->post('/add_reservation/{id_user_resa}', function (Request $request, Response $response, array $args) {
    if(isLogged()){
        $bdd = getPDO();
        $values = $request->getParsedBody();
        // // $values est un tableau associatif contenant les données
        // // Ici il faut faire qq chose avec les données lues !
        $insert_resa = $bdd->prepare("INSERT INTO Reservation (id_user_resa,id_vehicule,conducteur,date_debut, date_fin) VALUES(?,?,?,?,?);");
        $insert_resa->execute(array($args['id_user_resa'],$values['id_vehicule'],$values['id_conducteur'],$values['date_debut'], $values['date_fin']));
        $resa_id = $bdd->lastInsertId();
        $insert_participant = $bdd->prepare("INSERT INTO Participant (id_membre,id_reservation) VALUES(?,?);");
        foreach($values["participants"] as $participant){
            $insert_participant->execute(array($participant,$resa_id));
        }
        if($insert_resa && $insert_participant){
            $payload = json_encode(['reponse' => "ok","id"=>$resa_id],JSON_PRETTY_PRINT);
        }else{
            $payload = json_encode(['reponse' => 'nok'],JSON_PRETTY_PRINT);
        }
        $response->getBody()->write($payload);
    }
    else{
        $response->getBody()->write(json_encode(['reponse' => 'Error']));
    }
    
    return $response->withHeader('Content-Type','application/json');

});
$app->options('/add_reservation/{vehicule}/{conducteur}/{date_debut}/{date_fin}/', function (Request $request, Response $response, array $args) {
    return $response;
});



/**
 * Cette fonction permet de créer une réservation (méthode POST).
 * Lien POST : http://localhost:8001/reservations
 * Explication : Cette url permet de faire un POST. Pour réaliser cette opération il est nécessaire de préciser dans l'URL :
 * {véhicule} : Un numéro de véhicule (INT),
 * {date_debut} : Une date de début (DATE TIME = aaaa-mm-jj),
 * {date_fin} : Une date de fin (DATE TIME = aaaa-mm-jj),
 * {commentaire} : Enfin il faut ajouter un commentaire (qui peut-être NULL).
 */

 /**
 *  @OA\Get(
 *      path="/reservations",
 *      tags={"Reservation"},
 *      @OA\Response(
 *          response="200",
 *          description="Success",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="Login Success")  
 *          )   
 *      ),
 *      @OA\Response(
 *          response="500",
 *          description="Error",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="Login Error")  
 *          )   
 *      )
 * )
*/
$app->get('/reservations', function (Request $request, Response $response, array $args) {
    if(isLogged()){
        $bdd = getPDO();

        $recape_Reservations = $bdd->query("SELECT id_user_resa, date_debut, date_fin FROM Reservation");
    
        $reservations = array();
    
        foreach($recape_Reservations as $key=>$value){
            /** Search user */
            $search_user = $bdd->prepare("SELECT nom, prenom FROM Membre WHERE id_membre = ?;");
            $search_user->execute(array($value['id_user_resa'])); 
            $res = $search_user->fetch(PDO::FETCH_ASSOC);
            $value['user_resa'] = $res;
            array_push($reservations,$value);
        }
    
        $payload = json_encode($reservations, JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
    }
    else{
        $response->getBody()->write(json_encode(['reponse' => 'Error']));
    }
    return $response->withHeader('Content-Type','application/json');
});
$app->options('/reservations', function (Request $request, Response $response, array $args) {
    return $response;
});


/**
 * Cette fonction permet de modifier une réservation (méthode PUT).
 * Lien PUT : http://localhost:8001/edit_reservation/{id_reservation}/{vehicule}/{conducteur}/{date_debut}/{date_fin}
 * Explication : Cette url permet de faire un PUT. Pour réaliser cette opération il est nécessaire de préciser dans l'URL :
 * {id_reservation} : Un numéro de réservation (INT),
 * {vehicule} : Un id de véhicule (INT),
 * {conducteur} : Un id de membre (INT),
 * {date_debut} : Une date de début (DATE TIME = aaaa-mm-jj),
 * {date_fin} : Une date de fin (DATE TIME = aaaa-mm-jj),
 */
 /**
 *  @OA\Put(
 *      path="/edit_reservation/{id_reservation}/{vehicule}/{conducteur}/{date_debut}/{date_fin}",
 *      tags={"Reservation"},
 *      @OA\Parameter(
 *          name="vehicule",
 *          in="path",
 *          description="ID vehicule",
 *          required=true,
 *          @OA\Schema(type="integer")
 *      ),
 *      @OA\Parameter(
 *          name="id_reservation",
 *          in="path",
 *          description="ID reservation",
 *          required=true,
 *          @OA\Schema(type="integer")
 *      ),
 *      @OA\Parameter(
 *          name="conducteur",
 *          in="path",
 *          description="ID conducteur",
 *          required=true,
 *          @OA\Schema(type="integer")
 *      ),
 *      @OA\Parameter(
 *          name="date_debut",
 *          in="path",
 *          description="Debut reservation",
 *          required=true,
 *          @OA\Schema(type="string")
 *      ),
 *      @OA\Parameter(
 *          name="date_fin",
 *          in="path",
 *          description="Fin reservation",
 *          required=true,
 *          @OA\Schema(type="string")
 *      ),
 *      @OA\RequestBody(
 *          required=true,
 *          @OA\JsonContent(@OA\Items(
 *              type="object",
 *              required={"IDreservation"}
 *          ),
 *          ref="#/components/schemas/ReservationPut")
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="Success",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="Login Success")  
 *          )   
 *      ),
 *      @OA\Response(
 *          response="500",
 *          description="Error",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="Login Error")  
 *          )   
 *      )
 * )
*/
$app->post('/edit_reservation/{id_reservation}', function (Request $request, Response $response, array $args) {
    if(isLogged()){
        $bdd = getPDO();

        $values = $request->getParsedBody();
        if($values['vehicule'] != null){
            $update_resa = $bdd->prepare("UPDATE Reservation SET vehicule = ? WHERE id_reservation = ?");
            $update_resa->execute(array($values['vehicule'],$args['id_reservation']));
        }

        if($values['id_conducteur'] != null){
            $update_resa = $bdd->prepare("UPDATE Reservation SET conducteur = ? WHERE id_reservation = ?");
            $update_resa->execute(array($values['id_conducteur'],$args['id_reservation']));
        }

        if($values['date_debut'] != null){
            $update_resa = $bdd->prepare("UPDATE Reservation SET date_debut = ? WHERE id_reservation = ?");
            $update_resa->execute(array($values['date_debut'],$args['id_reservation']));
        }

        if($values['date_fin'] != null){
            $update_resa = $bdd->prepare("UPDATE Reservation SET date_fin = ? WHERE id_reservation = ?");
            $update_resa->execute(array($values['date_fin'],$args['id_reservation']));
        }

        if($values['commentaire'] != null){
            $update_resa = $bdd->prepare("UPDATE Reservation SET commentaire = ? WHERE id_reservation = ?");
            $update_resa->execute(array($values['commentaire'],$args['id_reservation']));
        }
        if($values['participants'] != null){
            $update_resa = $bdd->prepare("DELETE FROM Participant WHERE id_reservation = ?");
            $update_resa->execute(array($args['id_reservation']));
            $update_resa = $bdd->prepare("INSERT INTO Participant (id_membre,id_reservation) VALUES(?,?);");
            foreach($values['participants'] as $participant){
                $update_resa->execute(array($participant,$args['id_reservation']));
            }
        }

        if($update_resa){
            $payload = json_encode(['code' => 'OK'],JSON_PRETTY_PRINT);
        }else{
            $payload = json_encode(['code' => 'ERR'],JSON_PRETTY_PRINT);
        }
        $response->getBody()->write($payload);
    }
    else{
        $response->getBody()->write(json_encode(['reponse' => 'Error']));
    }
    return $response->withHeader('Content-Type','application/json');
});
$app->options('/edit_reservation/{id_reservation}', function (Request $request, Response $response, array $args) {
    return $response;
});


/**
 *  @OA\Delete(
 *      path="/reservation/{id_reservation}",
 *      tags={"Reservation"},
 *      @OA\Parameter(
 *          name="id_reservation",
 *          in="path",
 *          description="ID reservation",
 *          required=true,
 *          @OA\Schema(type="integer")
 *      ),
 *      @OA\Response(
 *          response="200",
 *          description="Success",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="OK")  
 *          )   
 *      ),
 *      @OA\Response(
 *          response="500",
 *          description="Error",
 *          @OA\JsonContent(
 *            type="object",
 *            @OA\Property(property="reponse", type="string", example="ERR")  
 *          )   
 *      )
 * )
*/
$app->delete('/reservation/{id_reservation}', function (Request $request, Response $response, array $args) {
    if(isLogged()){
        $bdd = getPDO();
        $deleteC = $bdd->prepare("UPDATE `Reservation` SET `is_delete`=1 WHERE id_reservation = ?");
        $deleteC->execute(array($args['id_reservation']));
        if($deleteC){
            $payload = json_encode(['code' => 'OK'],JSON_PRETTY_PRINT);
        }else{
            $payload = json_encode(['code' => 'ERR'],JSON_PRETTY_PRINT);
        }
        $response->getBody()->write($payload);
    }
    else{
        $response->getBody()->write(json_encode(['reponse' => 'Error']));
    }
    return $response->withHeader('Content-Type','application/json');
});
$app->options('/reservation/{id_reservation}', function (Request $request, Response $response, array $args) {
    return $response;
});
$app->get('/reservation/resaByCar/{id_voiture}', function (Request $request, Response $response, array $args) {
    if(isLogged()){
        $bdd = getPDO();
        $resaByCar = $bdd->prepare("SELECT * from `Reservation` where `id_vehicule`=? and `is_delete` IS NULL");
        $resaByCar->execute(array($args['id_voiture']));
        $res = $resaByCar->fetchAll(PDO::FETCH_OBJ);
        $payload = json_encode($res, JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
    }
    else{
        $response->getBody()->write(json_encode(['reponse' => 'Error']));
    }
    return $response->withHeader('Content-Type','application/json');
});
$app->options('/reservation/resaByCar/{id_voiture}', function (Request $request, Response $response, array $args) {
    return $response;
});
$app->get('/reservation/restitution/{id_utilisateur}', function (Request $request, Response $response, array $args) {
    if(isLogged()){
        $bdd = getPDO();
        $resa_rest = $bdd->prepare("SELECT  mo.label as modele, m.label marque, v.description, v.image, v.kilometrage, v.plein_essence, v.id_vehicule,r.conducteur,r.date_debut,r.date_fin,r.commentaire,v.plaque_immatriculation,r.id_reservation
                                    FROM `Reservation` r 
                                    INNER JOIN `Voiture` v 
                                    ON r.id_vehicule=v.id_vehicule
                                    INNER JOIN `Marque` m
                                    ON m.id_marque=v.marque
                                    INNER JOIN `Modele` mo
                                    ON  mo.id_modele = v.modele
                                    WHERE id_user_resa = ?");
        $resa_rest->execute(array($args['id_utilisateur']));
        $res = $resa_rest->fetchAll(PDO::FETCH_ASSOC);
        $payload = json_encode(['reponse'=>'ok','reservations' => $res],JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
    }
    else{
        $response->getBody()->write(json_encode(['reponse' => 'nok']));
    }
    return $response->withHeader('Content-Type','application/json');
});
$app->options('/reservation/restitution/{id_utilisateur}', function (Request $request, Response $response, array $args) {
    return $response;
});
$app->get('/reservation/selected/{id_resa}', function (Request $request, Response $response, array $args) {
    if(isLogged()){
        $bdd = getPDO();
        $participants = $bdd->prepare("SELECT m.id_membre FROM `Membre` m inner join `Participant` p on m.id_membre=p.id_membre where p.id_reservation=?");
        $participants->execute(array($args['id_resa']));
        $participant_resa = $participants->fetchAll();
        $resa_rest = $bdd->prepare("SELECT  mo.label as modele, m.label marque, v.description, v.image, v.kilometrage, v.plein_essence, v.id_vehicule,r.conducteur,r.date_debut,r.date_fin,r.commentaire,v.plaque_immatriculation,r.id_reservation
                                    FROM `Reservation` r 
                                    INNER JOIN `Voiture` v 
                                    ON r.id_vehicule=v.id_vehicule
                                    INNER JOIN `Marque` m
                                    ON m.id_marque=v.marque
                                    INNER JOIN `Modele` mo
                                    ON  mo.id_modele = v.modele
                                    WHERE id_reservation = ?");
        $resa_rest->execute(array($args['id_resa']));
        $resa_info = $resa_rest->fetch(PDO::FETCH_ASSOC);
        $payload = json_encode(['reponse'=>'ok','reservation' => $resa_info,'participants'=>$participant_resa],JSON_PRETTY_PRINT);
        $response->getBody()->write($payload);
    }
    else{
        $response->getBody()->write(json_encode(['reponse' => 'nok']));
    }
    return $response->withHeader('Content-Type','application/json');
});
$app->options('/reservation/selected/{id_resa}', function (Request $request, Response $response, array $args) {
    return $response;
});


