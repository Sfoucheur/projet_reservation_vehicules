import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { LoginGuardGuard} from '../app/guards/login-guard.guard';
import { IsloggedGuard } from '../app/guards/islogged.guard';


const routes: Routes = [
  {path:"",component:LoginComponent,canActivate:[IsloggedGuard]},
  {path:"home",component:HomeComponent,canActivate:[LoginGuardGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
