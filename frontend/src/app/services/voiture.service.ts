import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Voiture } from '../interfaces/voiture';

@Injectable({
  providedIn: 'root'
})
export class VoitureService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Access-Control-Allow-Origin':'*'   
    }),
    withCredentials: true
  };

  constructor(private HttpClient: HttpClient) { }


  getAllVoitures(){
    return this.HttpClient.get<Voiture>('http://localhost:8001/voitures',this.httpOptions).toPromise();
  }

  getVoituresByUserID(id_user: number){
    return this.HttpClient.get<Voiture>('http://localhost:8001/voitures/'+id_user,this.httpOptions).toPromise();
  }

  putPleinVoiture(id_vehicule: number, plein: boolean){
    return this.HttpClient.put<Voiture>('http://localhost:8001/voitures/' + id_vehicule + '/' + plein, this.httpOptions).toPromise();
  }
  getVoitureById(car_id:number){
    return this.HttpClient.get<Voiture>('http://localhost:8001/voiture/'+car_id,this.httpOptions).toPromise();
  }


  

}
