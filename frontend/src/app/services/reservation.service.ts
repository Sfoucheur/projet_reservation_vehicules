import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { Reservation } from '../interfaces/reservation';
import { DateInput } from '@fullcalendar/core/datelib/env';



@Injectable({
  providedIn: 'root'
})
export class ReservationService {
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Access-Control-Allow-Origin':'*'    
    }),
    withCredentials: true
  };


  constructor(private HttpClient: HttpClient) {}

  deleteReservation(id_reservation: number): Promise<Reservation> {
    return this.HttpClient.delete<Reservation>("http://localhost:8001/reservation/"+id_reservation, this.httpOptions).toPromise(); 
  }
  getAllReservations(){
    return this.HttpClient.get<Reservation>('http://localhost:8001/reservations',this.httpOptions).toPromise();
  }
  getReservationsByCar(id_car:string){
    return this.HttpClient.get('http://localhost:8001/reservation/resaByCar/'+id_car,this.httpOptions).toPromise();
  }
  addReservation(id_user_resa: number, id_vehicule: number, id_conducteur: string, date_debut: DateInput, date_fin: DateInput,participants:Array<any>): Promise<Reservation> {
    const body = {id_vehicule: id_vehicule, id_conducteur: id_conducteur, date_debut: date_debut, date_fin: date_fin,participants: participants}
    return this.HttpClient.post<Reservation>("http://localhost:8001/add_reservation/" + id_user_resa, body,this.httpOptions).toPromise();
  }
  updateReservation(id_reservation: number,id_conducteur: string = null, participants: Array<any> = null,date_debut: DateInput = null, date_fin: DateInput = null){
    const body = {id_conducteur: id_conducteur, participants: participants,date_debut: date_debut, date_fin: date_fin};
    return this.HttpClient.post<Reservation>("http://localhost:8001/edit_reservation/"+id_reservation,body,this.httpOptions).toPromise();  
  }
  getRestitutionById(id_user:number){
    return this.HttpClient.get('http://localhost:8001/reservation/restitution/'+id_user,this.httpOptions).toPromise();
  }
  getSelectedReservation(id_resa:number){
    return this.HttpClient.get('http://localhost:8001/reservation/selected/'+id_resa,this.httpOptions).toPromise();
  }
  


}
