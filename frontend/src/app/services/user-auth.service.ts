import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpHeaders
} from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UserAuthService {
  BASEURL = 'http://localhost:8001/user/';
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin':'*'    
    }),
    withCredentials: true
  };
  constructor(private http: HttpClient) {}

  login(username: string,password: string) {
    let body = {username:username,password:password};
    return this.http.post(this.BASEURL + 'login',body,this.httpOptions).toPromise();
  }
  whoIs(){
    return this.http.get(this.BASEURL + 'whoIs',this.httpOptions).toPromise();
  }
  checkAcces(){
    return this.http.get(this.BASEURL+'checkSession',this.httpOptions).toPromise();
  }
  allUsers(){
    return this.http.get(this.BASEURL+'all',this.httpOptions).toPromise();
  }
  

}
