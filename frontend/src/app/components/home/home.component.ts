import { Component, OnInit, ViewChild} from '@angular/core';
import timeGridPlugin from '@fullcalendar/timegrid';
import frLocale from '@fullcalendar/core/locales/fr';
import bootstrapPlugin from '@fullcalendar/bootstrap';
import { ReservationService } from '../../services/reservation.service';
import { VoitureService } from '../../services/voiture.service';
import interactionPlugin from '@fullcalendar/interaction';
import Swal from 'sweetalert2';
import { formatRange } from '@fullcalendar/core';
import { Voiture } from 'src/app/interfaces/voiture';
import { UserAuthService } from '../../services/user-auth.service';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { log } from 'util';




@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  today: string;
  data: Array <Voiture> = [];
  dataRest: Array <Voiture> = [];
  userName:string;
  userId:string;
  conducteur:string;
  car_id:number;
  users: Array <any> = [];
  participants: Array<number>;
  
  @ViewChild('calendar',{static:false}) calendarComponent;
  @ViewChild('calendrier',{static:false}) calendrier;
  @ViewChild('liste_voiture',{static:false}) liste_voiture;
  @ViewChild('restitution',{static:false}) restitution;

  configurationCalendar = {
    calendarPlugins : [timeGridPlugin,bootstrapPlugin,interactionPlugin],
    locale : frLocale,
    allDaySlot : false,
    slotDuration : "12:00:00",
    eventOverlap : false,
    height : 0,
    themeSystem : 'bootstrap',
    selectable : true,
    editable : true,
    slotEventOverlap : false,
    events : []
  }
  voitureRest = {
    modele: "",
    marque: "",
    description: "",
    image: "",
    kilometrage: "",
    kilometrage_fin:"",
    plein_essence: "",
    id_vehicule: "",
    conducteur: "",
    date_debut: "",
    date_fin: "",
    commentaire: null,
    plaque_immatriculation: "",
    id_reservation: ""
  }
  voitureAdd : {
    modele: string,
    marque: string,
    description: string,
    image: string,
    kilometrage: string,
    kilometrage_fin:string,
    plein_essence: string,
    id_vehicule: string,
    participant: Array<any>;
  }
  voitureModif : {
    modele: string,
    marque: string,
    description: string,
    image: string,
    conducteur:string,
    nbparticipant:Array<any>,
    kilometrage: string,
    kilometrage_fin:string,
    plein_essence: string,
    id_vehicule: string,
    participant: Array<any>;
  }
  constructor(private resa: ReservationService, private voit: VoitureService, private userService:UserAuthService, private modalService: NgbModal) { 
    this.participants=[];
    this.voitureAdd = {
      modele: "",
      marque: "",
      description: "",
      image: "",
      kilometrage: "",
      kilometrage_fin:"",
      plein_essence: "",
      id_vehicule: "",
      participant: []
    }
    this.voitureModif = {
      modele: "",
      marque: "",
      description: "",
      image: "",
      conducteur:"",
      nbparticipant:[],
      kilometrage: "",
      kilometrage_fin:"",
      plein_essence: "",
      id_vehicule: "",
      participant: []
    }
  }
  
  getDate(date){
    var dates = date.getFullYear()+ "-" +
    ("00" + (date.getMonth() + 1)).slice(-2) + "-" +
    ("00" + date.getDate()).slice(-2)+ " " +
    ("00" + date.getHours()).slice(-2) + ":" +
    ("00" + date.getMinutes()).slice(-2) + ":" +
    ("00" + date.getSeconds()).slice(-2);
    return dates;
  }
  getLastDay(){
    var curr = new Date;
    var lastday = new Date(curr.setDate(curr.getDate() - curr.getDay()+8));
    return lastday;
  }
  handleDateClick(arg,content) { // handler method
    arg.date = this.getDate(arg.date);
    var currentDay = new Date();
    if(arg.dateStr > currentDay.toISOString().split('T', 1)[0] && arg.dateStr <= this.getDate(this.getLastDay())){
      this.voit.getVoitureById(this.car_id).then((data:any)=>{
        this.voitureAdd = data;
        this.voitureAdd.participant = [];
        this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
          var heurefin = "11:59:59";
          if(arg.date.slice(11,13)>=12){
            heurefin = "23:59:59";
          }
          this.userService.whoIs().then((data:any)=>{
            if(this.conducteur==""){
              this.conducteur=this.userId;
              this.voitureAdd.participant.splice(this.voitureAdd.participant.indexOf(this.userId), 1 );
            }
            if(data.reponse!="nok"){
              var fin = arg.date.slice(0,10) + " " + heurefin;
              var debut = arg.date;
              var date_time_fin = arg.date.slice(0,10)+"T"+heurefin;
              var date_time_debut = arg.date.slice(0,10)+"T"+arg.date.slice(11,25);
              this.resa.addReservation(data.id,this.car_id,this.conducteur,debut,fin,this.voitureAdd.participant).then((data:any)=>{
                this.calendarComponent.calendar.addEvent({
                  title: this.voitureAdd.description,
                  start: date_time_debut,
                  end: date_time_fin,
                  id: data.id
                });
                Swal.fire(
                  'Confirmé !',
                  'Votre réservation a  été enregistrée',
                  'success'
                ).then((result) => {
                  this.voitureAdd.participant=[];
                  this.conducteur=this.userId;
                  this.participants = [1];
                  this.userService.whoIs().then((data:any)=>{
                    this.resa.getRestitutionById(data.id).then((data:any)=>{
                      this.dataRest = data.reservations;
                    })
                  });
                });
              })
            }
          })
        }, (reason) => {
          
        });
      });
    }       
  }
  onResizeDrag(info,eventType){
    var currentDay = new Date();
    var previousEvent = info.oldEvent;
    if(eventType=="resize"){
      previousEvent=info.prevEvent;
    }
    if(this.getDate(previousEvent.start).slice(0,10) >= currentDay.toISOString().split('T', 1)[0] && this.getDate(info.event.start).slice(0,10) >= currentDay.toISOString().split('T', 1)[0]){
      var str = formatRange(info.event.start, info.event.end, {
        month: 'long',
        year: 'numeric',
        day: 'numeric',
        separator: ' au ',
        locale: 'fr'
      })
      Swal.fire({
        title: 'Êtes vous sûre?',
        text: "Déplacer '" + info.event.title + "' : "+str,
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Oui',
        cancelButtonText: 'Non',
        reverseButtons: true,
      }).then((result) => {
        if (result.value && info.event.start>=this.calendarComponent.calendar.view.currentStart && info.event.end < this.calendarComponent.calendar.view.currentEnd) {
          var date_debut = this.getDate(info.event.start).slice(0,10)+"T"+this.getDate(info.event.start).slice(11,25);
          var date_fin = this.getDate(info.event.end).slice(0,10)+"T"+this.getDate(info.event.end).slice(11,25);
          this.resa.updateReservation(info.event.id,null,null,date_debut,date_fin).then((data:any)=>{
            if(data.code=="OK"){
              Swal.fire(
                'Déplacé!',
                'Votre réservation a été déplacée',
                'success'
              ).then((result) => {
                this.userService.whoIs().then((data:any)=>{
                  this.resa.getRestitutionById(data.id).then((data:any)=>{
                    this.dataRest = data.reservations;
                  })
                });
              });
            }else{
              info.revert();
              Swal.fire(
                'Erreur!',
                'Votre réservation n\'a pas été déplacée',
                'error'
              )
            }
          })
          
          
        } else if (
          /* Read more about handling dismissals below */
          result.dismiss === Swal.DismissReason.cancel
        ) {
          info.revert();
          Swal.fire(
            'Annulée',
            'Votre réservation n\'a pas été déplacée',
            'error'
          )
        }
        else{
          info.revert();
          Swal.fire(
            'Annulée',
            'Votre réservation n\'a pas été déplacée',
            'error'
          )
        }
      })
    }else{
      info.revert();
    }

  }
  getDays(dayRenderInfo) {
    var currentDay = new Date();
    currentDay.setDate(currentDay.getDate()-1);
    if(dayRenderInfo.date.toISOString().split('T', 1)[0] < currentDay.toISOString().split('T', 1)[0]){
      dayRenderInfo.el.style.backgroundColor = "rgba(128,128,128,0.4)";
    }else{
      dayRenderInfo.el.style.backgroundColor = "white";
    }
  }
  onCarClick(id_car){
    this.car_id=id_car;
    this.resa.getReservationsByCar(id_car).then((data:any)=>{
      var eventSources = this.calendarComponent.calendar.getEvents();
      for(event of eventSources){
        // @ts-ignore
        event.remove();
      }
      for(let elem of data){
        this.calendarComponent.calendar.addEvent({
          title: elem.commentaire,
          start: elem.date_debut,
          end: elem.date_fin,
          id: elem.id_reservation
        });
      }
      this.calendrier.nativeElement.className = 'displayCalendar';
      this.calendrier.nativeElement.scrollIntoView({behavior: 'smooth'}); 
    });
 
  }
  goToRestitution(){
    this.restitution.nativeElement.scrollIntoView({behavior: 'smooth'});
  }
  goToVoitures(){
    this.liste_voiture.nativeElement.scrollIntoView({behavior: 'smooth'});
  }
  ngOnInit() {
    this.participants.push(1);
    this.voitureModif.nbparticipant.push(1);
    const now = new Date;
    this.today =now.toISOString();
    this.userService.allUsers().then((data:any)=>{
      this.users = data;
    });
    this.voit.getAllVoitures().then((data:any)=>{
      this.data=data;
    });
    this.userService.whoIs().then((data:any)=>{
      this.userName = data.prenom;
      this.userId = data.id;
      this.conducteur=this.userId;
      this.resa.getRestitutionById(data.id).then((data:any)=>{
        this.dataRest = data.reservations;
      })
    });
  }
  open(content,datas) {
    this.voitureRest = datas;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    }, (reason) => {
      
    });
  }
  addElement(){
    if(this.participants.length<4)
    {
      this.participants.push(1);
    }
    
  }
  checkParticipant(user,conducteur){
    if(!conducteur && this.voitureAdd.participant.includes(user)){
      this.voitureAdd.participant.splice(this.voitureAdd.participant.findIndex(item => item === user), 1);
    }else if(!conducteur && this.conducteur==user){
      this.conducteur="";
    }else if(conducteur && this.voitureAdd.participant.includes(user)){
      this.voitureAdd.participant.splice(this.voitureAdd.participant.findIndex(item => item === user), 1);
    }
  }
  checkParticipantModif(user,conducteur){
    if(!conducteur && this.voitureModif.participant.includes(user)){
      this.voitureModif.participant.splice(this.voitureModif.participant.findIndex(item => item === user), 1);
    }else if(!conducteur && this.voitureModif.conducteur===user){
      this.voitureModif.conducteur="";
    }else if(conducteur && this.voitureModif.participant.includes(user)){
      this.voitureModif.participant.splice(this.voitureModif.participant.findIndex(item => item === user), 1);
    }
  }
  saveRestitution(){

  }
  modifElement(){
    if(this.voitureModif.nbparticipant.length<4)
    {
      this.voitureModif.nbparticipant.push(1);
    }
  }
  modifyEvent(eventClickInfo,content){
    Swal.fire({
      title: 'Que voulez vous faire ?',
      icon: 'question',
      showCancelButton: true,
      confirmButtonText: 'Supprimer',
      cancelButtonText: 'Modifier',
      reverseButtons: true
    }).then((result) => {
      if (result.value) {
        this.resa.deleteReservation(eventClickInfo.event.id).then((data:any)=>{
          eventClickInfo.event.remove();
        });
        Swal.fire(
          'Supprimé!',
          'La réservation a bien été supprimée',
          'success'
        )
      } else if (
        /* Read more about handling dismissals below */
        result.dismiss === Swal.DismissReason.cancel
      ) {
        this.resa.getSelectedReservation(eventClickInfo.event.id).then((data:any)=>{
          this.voitureModif.nbparticipant = [1];
          var array = [];
          for(let elem of data.participants){
            array.push(elem.id_membre);
          }
          this.voitureModif.participant = array;
          this.voitureModif.description = data.reservation.description;
          this.voitureModif.conducteur = data.reservation.conducteur;
          for(let i =1;i< this.voitureModif.participant.length;i++){
            this.voitureModif.nbparticipant.push(1);
          }
          this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
            if(this.voitureModif.conducteur==""){
              this.voitureModif.conducteur=this.userId;
              this.voitureModif.participant.splice(this.voitureModif.participant.indexOf(this.userId), 1 );
            }
            this.resa.updateReservation(eventClickInfo.event.id,this.voitureModif.conducteur,this.voitureModif.participant).then((data:any)=>{
              Swal.fire(
                'Mofifiéé!',
                'La réservation a bien été modifiée',
                'success'
              )
            })
          }, (reason) => {
            
          });
        });
        
      }
    })
    
  }

}
