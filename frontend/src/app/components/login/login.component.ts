import { Component, OnInit } from '@angular/core';
import { UserAuthService } from '../../services/user-auth.service';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  model:{username:string,password:string}
  constructor(private userApi:UserAuthService,private router:Router) { 
    this.model = {username:"",password:""};
  }

  ngOnInit() {
    this.userApi.checkAcces().then((data:any) =>{
      if( data.reponse == "ok")
        this.router.navigate(['home']);
    },
    err => {
      this.router.navigate(['']);
    });
  }
  connexion(){
    this.userApi.login(this.model.username,this.model.password).then(
    (data:any) => {
      console.log("Login : ",data);
      this.router.navigate(["home"]);
    },
    err=>{
      console.log(err);
    });
  };
}
