import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { UserAuthService } from '../services/user-auth.service';

@Injectable({
  providedIn: 'root'
})
export class IsloggedGuard implements CanActivate {
  constructor(private userAuthService: UserAuthService, private router: Router) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return this.userAuthService.checkAcces().then((data:any) =>{
        if(data.reponse == "ok")
          this.router.navigate(['home']);
        return data.reponse != "ok";
      },
      err => {
        this.router.navigate(['']);
        return false;
      });
  }
  
}
