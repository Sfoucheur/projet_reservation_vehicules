import { DateInput } from '@fullcalendar/core/datelib/env';

export interface Reservation {
    "id_reservation": number,
    "id_user_resa": number,
    "id_vehicule": number,
    "conducteur": number,
    "date_debut": DateInput,
    "date_fin": DateInput,
    "commentaire": string,
    "is_delete": boolean
  }