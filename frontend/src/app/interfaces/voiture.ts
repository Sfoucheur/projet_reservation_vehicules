export interface Voiture {
    "id_vehicule": number,
    "modele": number,
    "marque": number,
    "plaque_immatriculation": string,
    "description": string,
    "kilometrage": number,
    "plein_essence": boolean
  }