# Projet Reservation de Voiture

## Pour Swagger

`Tutoriel vidéo : https://www.youtube.com/watch?v=no0y4ISItiw`

`Doc Swagger : https://swagger.io/docs/specification/about/`

## Installer les dépendences

A la racine du projet :

Aller dans le dossier `Frontend` et Lancer `npm install`

Aller dans le dossier `Backend` et Lancer `composer install`

## Lancer les conteneurs et accéder aux sites

A la racine du projet :

Lancer `docker-compose up --build` pour construire les containers

### Sur le serveur

Acceder à angular :

Au lancement des containers ctrl cliquer sur le `localhost:4200` lors du lancement du container `frontend-angular`

Acceder à phpmyadmin:

url : `http://prenom.lpweb-lannion.fr:4203`

Acceder à slim:

url : `http://prenom.lpweb-lannion.fr:8001`

### En local

Acceder à angular :

url : `localhost:4200`

Acceder à phpmyadmin:

url : `localhost:4203`

Acceder à slim:

url : `localhost:8001`


